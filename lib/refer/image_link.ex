defmodule Refer.ImageLink do
  require Logger
  alias Refer.{AdminNotifier, Deals}

  def validate_all do
    Deals.list_cards()
    |> Enum.reduce([], fn card, cards_with_bad_links ->
      case check_url(card.image_url) do
        200 ->
          cards_with_bad_links

        image_url_status ->
          [{card, image_url_status} | cards_with_bad_links]
      end
    end)
    |> Enum.each(fn card_with_status ->
      AdminNotifier.report_broken_link(card_with_status)
    end)

    Logger.info("All image links checked ✅")
  end

  def check_url(url) do
    Logger.info("🌍 Checking url: #{url}")

    get_status_code(url)
  end

  def get_status_code(url) do
    case Req.get(url) do
      {:ok, %Req.Response{status: status_code}} ->
        Logger.info("Req status code: #{status_code}")
        status_code

      _ ->
        :no_code
    end
  end
end
