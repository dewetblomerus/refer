defmodule Refer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Refer.Repo,
      Refer.MigrationRunner,
      ReferWeb.Telemetry,
      {Phoenix.PubSub, name: Refer.PubSub},
      ReferWeb.Endpoint,
      Refer.ImageLinkChecker
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Refer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ReferWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
