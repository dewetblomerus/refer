defmodule Refer.Deals.Card do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cards" do
    field :image_url, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(card, attrs) do
    card
    |> cast(attrs, [:image_url, :title])
    |> validate_required([:image_url, :title])
    |> unique_constraint(:title, name: :cards_title_index)
    |> unique_constraint(:image_url, name: :cards_image_url_index)
  end
end
