defmodule Refer.Deals.ReferralLink do
  use Ecto.Schema
  import Ecto.Changeset

  schema "referral_links" do
    field :url, :string
    field :base_url_id, :id
    field :user_id, :id
    field :card_id, :id

    timestamps()
  end

  @doc false
  def changeset(referral_link, attrs) do
    referral_link
    |> cast(attrs, [:url, :card_id, :user_id])
    |> validate_required([:url, :card_id, :user_id])
  end
end
