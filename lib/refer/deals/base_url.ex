defmodule Refer.Deals.BaseUrl do
  use Ecto.Schema
  import Ecto.Changeset

  schema "base_urls" do
    field :active, :boolean, default: false
    field :url, :string

    timestamps()
  end

  @doc false
  def changeset(base_url, attrs) do
    base_url
    |> cast(attrs, [:url, :active])
    |> validate_required([:url, :active])
  end
end
