defmodule Refer.ImageLinkChecker do
  use GenServer
  require Logger
  alias Refer.ImageLink

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    check_links()

    {:ok, state}
  end

  def handle_info(:perform, state) do
    check_links()

    {:ok, state}
  end

  defp check_links do
    ImageLink.validate_all()

    Process.send_after(self(), :perform, :timer.hours(1))
  end
end
