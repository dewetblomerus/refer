defmodule Refer.MigrationRunner do
  require Logger
  use Task

  def start_link(_) do
    Logger.info("Running Migrations 🛢")
    Refer.Release.migrate()
    Task.start_link(__MODULE__, :run, [])
  end

  def run do
    Logger.info("Migrations ran ✅")
  end
end
