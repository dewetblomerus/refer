defmodule Refer.AdminNotifier do
  require Logger
  import Swoosh.Email
  alias Refer.Deals.Card
  alias Refer.Mailer

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject, body) do
    email =
      new()
      |> to(recipient)
      |> from({"ReferralLink.me", admin_email()})
      |> subject(subject)
      |> text_body(body)

    with {:ok, _metadata} <- Mailer.deliver(email) do
      {:ok, email}
    end
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def report_broken_link({%Card{} = card, status_code}) do
    Logger.error(
      "⛔️ Image url for #{card.title} is broken #{card.image_url} with http status code #{status_code}"
    )

    deliver(admin_email(), "Broken image link", """
    ⛔️ Image url for #{card.title} with id #{card.id} is broken.

    #{card.image_url}

    Received status code #{status_code}"

    <img
      src=https://referrallink.me/images/referrallink_logo-b5282107b2a86a3fac09812cc4b1b1c3.png
      height="70"
      alt="Referrallink.me Logo"
    />
    """)
  end

  defp admin_email do
    "dewetblomerus@gmail.com"
  end
end
