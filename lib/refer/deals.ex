defmodule Refer.Deals do
  @moduledoc """
  The Deals context.
  """

  import Ecto.Query, warn: false
  alias Refer.Repo

  alias Refer.Accounts.User
  alias Refer.Deals.Card

  @doc """
  Returns the list of cards.

  ## Examples

      iex> list_cards()
      [%Card{}, ...]

  """
  def list_cards do
    Card
    |> order_by([c], [c.title])
    |> Repo.all()
  end

  def search_cards(search_phrase) do
    order_by = [
      asc: dynamic([c], fragment("SIMILARITY(?, ?)", c.title, ^search_phrase))
    ]

    from(
      c in Card,
      where:
        fragment(
          "SIMILARITY(?, ?) > 0.04",
          c.title,
          ^search_phrase
        ),
      order_by: ^order_by
    )
    |> reverse_order()
    |> Repo.all()
  end

  @doc """
  Gets a single card.

  Raises `Ecto.NoResultsError` if the Card does not exist.

  ## Examples

      iex> get_card!(123)
      %Card{}

      iex> get_card!(456)
      ** (Ecto.NoResultsError)

  """
  def get_card!(id), do: Repo.get!(Card, id)

  @doc """
  Creates a card.

  ## Examples

      iex> create_card(%{field: value})
      {:ok, %Card{}}

      iex> create_card(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_card(attrs \\ %{}) do
    %Card{}
    |> Card.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a card.

  ## Examples

      iex> update_card(card, %{field: new_value})
      {:ok, %Card{}}

      iex> update_card(card, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_card(%Card{} = card, attrs) do
    card
    |> Card.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a card.

  ## Examples

      iex> delete_card(card)
      {:ok, %Card{}}

      iex> delete_card(card)
      {:error, %Ecto.Changeset{}}

  """
  def delete_card(%Card{} = card) do
    Repo.delete(card)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking card changes.

  ## Examples

      iex> change_card(card)
      %Ecto.Changeset{data: %Card{}}

  """
  def change_card(%Card{} = card, attrs \\ %{}) do
    Card.changeset(card, attrs)
  end

  alias Refer.Deals.BaseUrl

  @doc """
  Returns the list of base_urls.

  ## Examples

      iex> list_base_urls()
      [%BaseUrl{}, ...]

  """
  def list_base_urls do
    Repo.all(BaseUrl)
  end

  @doc """
  Gets a single base_url.

  Raises `Ecto.NoResultsError` if the Base url does not exist.

  ## Examples

      iex> get_base_url!(123)
      %BaseUrl{}

      iex> get_base_url!(456)
      ** (Ecto.NoResultsError)

  """
  def get_base_url!(id), do: Repo.get!(BaseUrl, id)

  @doc """
  Creates a base_url.

  ## Examples

      iex> create_base_url(%{field: value})
      {:ok, %BaseUrl{}}

      iex> create_base_url(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_base_url(attrs \\ %{}) do
    %BaseUrl{}
    |> BaseUrl.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a base_url.

  ## Examples

      iex> update_base_url(base_url, %{field: new_value})
      {:ok, %BaseUrl{}}

      iex> update_base_url(base_url, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_base_url(%BaseUrl{} = base_url, attrs) do
    base_url
    |> BaseUrl.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a base_url.

  ## Examples

      iex> delete_base_url(base_url)
      {:ok, %BaseUrl{}}

      iex> delete_base_url(base_url)
      {:error, %Ecto.Changeset{}}

  """
  def delete_base_url(%BaseUrl{} = base_url) do
    Repo.delete(base_url)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking base_url changes.

  ## Examples

      iex> change_base_url(base_url)
      %Ecto.Changeset{data: %BaseUrl{}}

  """
  def change_base_url(%BaseUrl{} = base_url, attrs \\ %{}) do
    BaseUrl.changeset(base_url, attrs)
  end

  alias Refer.Deals.ReferralLink

  @doc """
  Returns the list of referral_links.

  ## Examples

      iex> list_referral_links()
      [%ReferralLink{}, ...]

  """
  def list_referral_links do
    Repo.all(ReferralLink)
  end

  def list_referral_links_for_card(card_id) do
    query =
      from r in ReferralLink,
        where: r.card_id == ^card_id,
        join: u in User,
        on: u.id == r.user_id,
        select: {r, u.first_name, u.last_name}

    Repo.all(query)
  end

  @doc """
  Gets a single referral_link.

  Raises `Ecto.NoResultsError` if the Referral link does not exist.

  ## Examples

      iex> get_referral_link!(123)
      %ReferralLink{}

      iex> get_referral_link!(456)
      ** (Ecto.NoResultsError)

  """
  def get_referral_link!(id), do: Repo.get!(ReferralLink, id)

  def get_referral_link_by(user_id: user_id, card_id: card_id) do
    query =
      from r in ReferralLink,
        where: r.user_id == ^user_id and r.card_id == ^card_id

    Repo.one(query)
  end

  @doc """
  Creates a referral_link.

  ## Examples

      iex> create_referral_link(%{field: value})
      {:ok, %ReferralLink{}}

      iex> create_referral_link(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_referral_link(attrs \\ %{}) do
    %ReferralLink{}
    |> ReferralLink.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a referral_link.

  ## Examples

      iex> update_referral_link(referral_link, %{field: new_value})
      {:ok, %ReferralLink{}}

      iex> update_referral_link(referral_link, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_referral_link(%ReferralLink{} = referral_link, attrs) do
    referral_link
    |> ReferralLink.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a referral_link.

  ## Examples

      iex> delete_referral_link(referral_link)
      {:ok, %ReferralLink{}}

      iex> delete_referral_link(referral_link)
      {:error, %Ecto.Changeset{}}

  """
  def delete_referral_link(%ReferralLink{} = referral_link) do
    Repo.delete(referral_link)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking referral_link changes.

  ## Examples

      iex> change_referral_link(referral_link)
      %Ecto.Changeset{data: %ReferralLink{}}

  """
  def change_referral_link(%ReferralLink{} = referral_link, attrs \\ %{}) do
    ReferralLink.changeset(referral_link, attrs)
  end
end
