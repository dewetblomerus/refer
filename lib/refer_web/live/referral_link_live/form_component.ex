defmodule ReferWeb.ReferralLinkLive.FormComponent do
  use ReferWeb, :live_component

  alias Refer.Accounts
  alias Refer.Deals

  @impl true
  def update(%{referral_link: referral_link} = assigns, socket) do
    changeset = Deals.change_referral_link(referral_link)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event(
        "validate",
        %{"referral_link" => referral_link_params},
        socket
      ) do
    params_with_user =
      Map.put(referral_link_params, "user_id", socket.assigns.user_id)

    changeset =
      socket.assigns.referral_link
      |> Deals.change_referral_link(params_with_user)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"referral_link" => referral_link_params}, socket) do
    params_with_user =
      Map.put(referral_link_params, "user_id", socket.assigns.user_id)

    save_referral_link(socket, socket.assigns.action, params_with_user)
  end

  defp save_referral_link(socket, :edit_referral_link, referral_link_params) do
    case Deals.update_referral_link(
           socket.assigns.referral_link,
           referral_link_params
         ) do
      {:ok, _referral_link} ->
        {:noreply,
         socket
         |> put_flash(:info, "Referral link updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_referral_link(socket, :new, referral_link_params) do
    case Deals.create_referral_link(referral_link_params) do
      {:ok, _referral_link} ->
        {:noreply,
         socket
         |> put_flash(:info, "Referral link created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp save_referral_link(socket, :new_referral_link, referral_link_params) do
    case Deals.create_referral_link(referral_link_params) do
      {:ok, _referral_link} ->
        {:noreply,
         socket
         |> put_flash(:info, "Referral link created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
