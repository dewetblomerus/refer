defmodule ReferWeb.ReferralLinkLive.IndexComponent do
  use ReferWeb, :live_component

  alias Refer.Deals

  @impl true
  def update(
        %{
          card: card,
          your_referral_link: _needed_for_refresh
        },
        socket
      ) do
    {:ok,
     assign(socket, %{
       referral_links: list_referral_links(card.id),
       card: card
     })}
  end

  defp list_referral_links(card_id) do
    Deals.list_referral_links_for_card(card_id)
  end
end
