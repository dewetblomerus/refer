defmodule ReferWeb.ReferralLinkLive.YourLinkComponent do
  use ReferWeb, :live_component

  alias Refer.Deals.ReferralLink

  @impl true
  def update(
        %{
          card: card,
          current_user: current_user,
          live_action: live_action,
          referral_link: referral_link
        },
        socket
      ) do
    {:ok,
     socket
     |> assign(%{
       referral_link: referral_link,
       live_action: live_action,
       current_user: current_user,
       card: card
     })
     |> apply_action(live_action)}
  end

  defp apply_action(socket, :new_referral_link) do
    socket
    |> assign(:page_title, "Share Your Referral Link")
    |> assign(:referral_link, %ReferralLink{})
  end

  defp apply_action(socket, :edit_referral_link) do
    socket
    |> assign(:page_title, "Edit Your Referral Link")
  end

  defp apply_action(socket, _), do: socket

  defp existing_link(nil), do: nil

  defp existing_link(referral_link) do
    referral_link.id != nil
  end
end
