defmodule ReferWeb.CardLive.Show do
  use ReferWeb, :live_view

  alias Refer.Accounts
  alias Refer.Deals

  @impl true
  def mount(_params, session, socket) do
    current_user = Accounts.get_user_by_session_token(session["user_token"])

    current_user_is_admin = Accounts.admin?(current_user)
    current_user_is_confirmed = Accounts.confirmed?(current_user)

    {:ok,
     assign(socket,
       current_user_is_admin: current_user_is_admin,
       current_user_is_confirmed: current_user_is_confirmed,
       current_user: current_user,
       your_referral_link: nil
     )}
  end

  @impl true
  def handle_params(%{"card_id" => card_id}, _, socket) do
    your_referral_link =
      get_referral_link_for(
        user_id: id_for(socket.assigns.current_user),
        card_id: card_id
      )

    {:noreply,
     assign(socket,
       page_title: page_title(socket.assigns.live_action),
       card: Deals.get_card!(card_id),
       your_referral_link: your_referral_link
     )}
  end

  defp get_referral_link_for(user_id: nil, card_id: _), do: nil

  defp get_referral_link_for(user_id: user_id, card_id: card_id) do
    Deals.get_referral_link_by(
      user_id: user_id,
      card_id: card_id
    )
  end

  defp id_for(nil), do: nil

  defp id_for(record) do
    record.id
  end

  @impl true
  def handle_event("delete_your_link", %{"id" => id}, socket) do
    referral_link = Deals.get_referral_link!(id)
    {:ok, _} = Deals.delete_referral_link(referral_link)

    {:noreply,
     assign(socket,
       your_referral_link: nil
     )}
  end

  @impl true
  def handle_event(
        "resend_confirmation",
        _,
        %{assigns: %{current_user: user}} = socket
      ) do
    Accounts.deliver_user_confirmation_instructions(
      user,
      &Routes.user_confirmation_url(socket, :edit, &1)
    )

    {:noreply,
     put_flash(socket, :info, "Confirmation email sent to #{user.email}")}
  end

  defp page_title(:show), do: "Show Card"
  defp page_title(:edit), do: "Edit Card"
  defp page_title(:new_referral_link), do: "Share Your Link"
  defp page_title(:edit_referral_link), do: "Edit Your Link"
end
