defmodule ReferWeb.CardLive.Index do
  require IEx
  use ReferWeb, :live_view

  alias Ecto.Changeset
  alias Refer.Accounts
  alias Refer.Deals
  alias Refer.Deals.Card

  @impl true
  def mount(_params, session, socket) do
    current_user_is_admin =
      Accounts.get_user_by_session_token(session["user_token"])
      |> Accounts.admin?()

    {:ok,
     assign(socket,
       cards: list_cards(),
       current_user_is_admin: current_user_is_admin,
       search_changeset: search_changeset()
     )}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    ensure_admin(socket)

    socket
    |> assign(:page_title, "Edit Card")
    |> assign(:card, Deals.get_card!(id))
  end

  defp apply_action(socket, :new, _params) do
    ensure_admin(socket)

    socket
    |> assign(:page_title, "New Card")
    |> assign(:card, %Card{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Cards")
    |> assign(:card, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    ensure_admin(socket)

    card = Deals.get_card!(id)
    {:ok, _} = Deals.delete_card(card)

    {:noreply, assign(socket, :cards, list_cards())}
  end

  @impl true
  def handle_event("search", %{"search" => %{"search_phrase" => ""}}, socket) do
    {:noreply, assign(socket, :cards, list_cards())}
  end

  @impl true
  def handle_event("search", %{"search" => search}, socket) do
    search
    |> search_changeset()
    |> case do
      %{valid?: true, changes: %{search_phrase: search_phrase}} ->
        {:noreply, assign(socket, :cards, Deals.search_cards(search_phrase))}

      _ ->
        {:noreply, socket}
    end
  end

  defp list_cards do
    Deals.list_cards()
  end

  defp ensure_admin(%{assigns: %{current_user_is_admin: true}}), do: nil

  @types %{search_phrase: :string}

  defp search_changeset(attrs \\ %{}) do
    Changeset.cast(
      {%{}, @types},
      attrs,
      [:search_phrase]
    )
    |> Changeset.validate_required([:search_phrase])
    |> Changeset.update_change(:search_phrase, &String.trim/1)
    |> Changeset.validate_length(:search_phrase, min: 2)
    |> Changeset.validate_format(:search_phrase, ~r/[A-Za-z0-9\ ]/)
  end
end
