defmodule ReferWeb.BaseUrlLive.Show do
  use ReferWeb, :live_view

  alias Refer.Deals

  @impl true
  def mount(_, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:base_url, Deals.get_base_url!(id))}
  end

  defp page_title(:show), do: "Show Base url"
  defp page_title(:edit), do: "Edit Base url"
end
