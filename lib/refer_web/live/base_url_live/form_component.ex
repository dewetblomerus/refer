defmodule ReferWeb.BaseUrlLive.FormComponent do
  use ReferWeb, :live_component

  alias Refer.Deals

  @impl true
  def update(%{base_url: base_url} = assigns, socket) do
    changeset = Deals.change_base_url(base_url)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"base_url" => base_url_params}, socket) do
    changeset =
      socket.assigns.base_url
      |> Deals.change_base_url(base_url_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"base_url" => base_url_params}, socket) do
    save_base_url(socket, socket.assigns.action, base_url_params)
  end

  defp save_base_url(socket, :edit, base_url_params) do
    case Deals.update_base_url(socket.assigns.base_url, base_url_params) do
      {:ok, _base_url} ->
        {:noreply,
         socket
         |> put_flash(:info, "Base url updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_base_url(socket, :new, base_url_params) do
    case Deals.create_base_url(base_url_params) do
      {:ok, _base_url} ->
        {:noreply,
         socket
         |> put_flash(:info, "Base url created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
