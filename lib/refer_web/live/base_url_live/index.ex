defmodule ReferWeb.BaseUrlLive.Index do
  use ReferWeb, :live_view

  alias Refer.Deals
  alias Refer.Deals.BaseUrl

  @impl true
  def mount(_, _session, socket) do
    {:ok,
     assign(socket, %{
       base_urls: list_base_urls()
     })}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Base url")
    |> assign(:base_url, Deals.get_base_url!(id))
  end

  defp apply_action(socket, :new, _) do
    socket
    |> assign(:page_title, "New Base url")
    |> assign(:base_url, %BaseUrl{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Base urls")
    |> assign(:base_url, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    base_url = Deals.get_base_url!(id)
    {:ok, _} = Deals.delete_base_url(base_url)

    {:noreply, assign(socket, :base_urls, list_base_urls())}
  end

  defp list_base_urls do
    Deals.list_base_urls()
  end
end
