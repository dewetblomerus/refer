defmodule ReferWeb.Router do
  use ReferWeb, :router

  import ReferWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ReferWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ReferWeb do
    pipe_through :browser

    live "/", CardLive.Index, :index
    live "/cards", CardLive.Index, :index

    live "/cards/:card_id", CardLive.Show, :show
    live "/cards/:card_id/referral_links/new", CardLive.Show, :new_referral_link

    live "/cards/:card_id/referral_links/:id/edit",
         CardLive.Show,
         :edit_referral_link
  end

  scope "/admin", ReferWeb do
    pipe_through [:browser, :require_admin]

    live "/cards/new", CardLive.Index, :new
    live "/cards/:id/edit", CardLive.Index, :edit

    live "/cards/:card_id/show/edit", CardLive.Show, :edit

    live "/base_urls", BaseUrlLive.Index, :index
    live "/base_urls/new", BaseUrlLive.Index, :new
    live "/base_urls/:id/edit", BaseUrlLive.Index, :edit

    live "/base_urls/:id", BaseUrlLive.Show, :show
    live "/base_urls/:id/show/edit", BaseUrlLive.Show, :edit

    import Phoenix.LiveDashboard.Router
    live_dashboard("/dashboard", metrics: ReferWeb.Telemetry)
  end

  # Other scopes may use custom stacks.
  # scope "/api", ReferWeb do
  #   pipe_through :api
  # end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through(:browser)

      forward("/mailbox", Plug.Swoosh.MailboxPreview)
    end
  end

  ## Authentication routes

  scope "/", ReferWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", ReferWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update

    get "/users/settings/confirm_email/:token",
        UserSettingsController,
        :confirm_email
  end

  scope "/", ReferWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
  end
end
