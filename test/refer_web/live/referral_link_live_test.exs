defmodule ReferWeb.ReferralLinkLiveTest do
  use ReferWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Refer.DealsFixtures

  @create_attrs %{url: "some url"}
  @update_attrs %{url: "some updated url"}
  @invalid_attrs %{url: nil}

  defp create_referral_link(params) do
    card = card_fixture()

    referral_link =
      referral_link_fixture(%{card_id: card.id, user_id: params.user.id})

    Map.merge(params, %{referral_link: referral_link, card: card})
  end

  describe "Index without your referral link and confirmed user" do
    setup [:register_and_log_in_user, :confirm_user]

    test "saves new referral_link", %{conn: conn} do
      card = card_fixture()

      {:ok, index_live, _html} =
        live(conn, Routes.card_show_path(conn, :show, card))

      assert index_live
             |> element("a", "Share Your Referral Link")
             |> render_click() =~
               "Share Your Referral Link"

      assert_patch(
        index_live,
        Routes.card_show_path(conn, :new_referral_link, card)
      )

      assert index_live
             |> form("#referral_link-form", referral_link: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#referral_link-form", referral_link: @create_attrs)
        |> render_submit()
        |> follow_redirect(
          conn,
          Routes.card_show_path(conn, :show, card)
        )

      assert html =~ "Referral link created successfully"
      assert html =~ "some url"
    end
  end

  describe "Index logged in but without confirming your email address" do
    setup [:register_and_log_in_user]

    test "saves new referral_link", %{conn: conn} do
      card = card_fixture()

      {:ok, _, html} = live(conn, Routes.card_show_path(conn, :show, card))

      assert html =~ "Confirm your email to share your link"
      refute html =~ "Share Your Referral Link"
    end
  end

  describe "Index with your referral link" do
    setup [:register_and_log_in_user, :confirm_user, :create_referral_link]

    test "lists all referral_links for a card", %{
      conn: conn,
      referral_link: referral_link,
      card: card
    } do
      {:ok, _index_live, html} =
        live(conn, Routes.card_show_path(conn, :show, card))

      assert html =~ card.title
      assert html =~ referral_link.url
    end

    test "updates referral_link in listing", %{
      card: card,
      conn: conn,
      referral_link: referral_link
    } do
      {:ok, index_live, _html} =
        live(conn, Routes.card_show_path(conn, :show, card))

      assert index_live
             |> element("#edit_your_link", "Edit")
             |> render_click() =~
               "Edit Your Referral Link"

      assert_patch(
        index_live,
        Routes.card_show_path(conn, :edit_referral_link, card, referral_link)
      )

      assert index_live
             |> form("#referral_link-form", referral_link: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#referral_link-form", referral_link: @update_attrs)
        |> render_submit()
        |> follow_redirect(
          conn,
          Routes.card_show_path(conn, :show, card)
        )

      assert html =~ "Referral link updated successfully"
      assert html =~ "some updated url"
    end

    test "deletes referral_link in listing", %{
      card: card,
      conn: conn,
      referral_link: referral_link
    } do
      {:ok, index_live, _html} =
        live(conn, Routes.card_show_path(conn, :show, card))

      assert index_live
             |> element("#delete_your_link", "Delete")
             |> render_click()

      refute has_element?(index_live, "#referral_link-#{referral_link.id}")
    end
  end
end
