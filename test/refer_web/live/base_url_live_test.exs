defmodule ReferWeb.BaseUrlLiveTest do
  use ReferWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Refer.DealsFixtures

  @create_attrs %{active: true, url: "some url"}
  @update_attrs %{active: false, url: "some updated url"}
  @invalid_attrs %{active: false, url: nil}

  defp create_base_url(_) do
    card = card_fixture()
    base_url = base_url_fixture()
    %{base_url: base_url, card_id: card.id}
  end

  describe "Index" do
    setup [:create_base_url, :register_and_log_in_admin]

    test "lists all base_urls", %{
      conn: conn,
      base_url: base_url
    } do
      {:ok, _index_live, html} =
        live(conn, Routes.base_url_index_path(conn, :index))

      assert html =~ "Listing Base urls"
      assert html =~ base_url.url
    end

    test "saves new base_url", %{conn: conn} do
      {:ok, index_live, _html} =
        live(conn, Routes.base_url_index_path(conn, :index))

      assert index_live |> element("a", "New Base url") |> render_click() =~
               "New Base url"

      assert_patch(
        index_live,
        Routes.base_url_index_path(conn, :new)
      )

      assert index_live
             |> form("#base_url-form", base_url: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#base_url-form", base_url: @create_attrs)
        |> render_submit()
        |> follow_redirect(
          conn,
          Routes.base_url_index_path(conn, :index)
        )

      assert html =~ "Base url created successfully"
      assert html =~ "some url"
    end

    test "updates base_url in listing", %{
      conn: conn,
      base_url: base_url
    } do
      {:ok, index_live, _html} =
        live(conn, Routes.base_url_index_path(conn, :index))

      assert index_live
             |> element("#base_url-#{base_url.id} a", "Edit")
             |> render_click() =~
               "Edit Base url"

      assert_patch(
        index_live,
        Routes.base_url_index_path(conn, :edit, base_url)
      )

      assert index_live
             |> form("#base_url-form", base_url: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#base_url-form", base_url: @update_attrs)
        |> render_submit()
        |> follow_redirect(
          conn,
          Routes.base_url_index_path(conn, :index)
        )

      assert html =~ "Base url updated successfully"
      assert html =~ "some updated url"
    end

    test "deletes base_url in listing", %{conn: conn, base_url: base_url} do
      {:ok, index_live, _html} =
        live(conn, Routes.base_url_index_path(conn, :index))

      assert index_live
             |> element("#base_url-#{base_url.id} a", "Delete")
             |> render_click()

      refute has_element?(index_live, "#base_url-#{base_url.id}")
    end
  end

  describe "Show" do
    setup [:create_base_url, :register_and_log_in_admin]

    test "displays base_url", %{
      conn: conn,
      base_url: base_url
    } do
      {:ok, _show_live, html} =
        live(
          conn,
          Routes.base_url_show_path(conn, :show, base_url)
        )

      assert html =~ "Show Base url"
      assert html =~ base_url.url
    end

    test "updates base_url within modal", %{conn: conn, base_url: base_url} do
      {:ok, show_live, _html} =
        live(
          conn,
          Routes.base_url_show_path(conn, :show, base_url)
        )

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Base url"

      assert_patch(show_live, Routes.base_url_show_path(conn, :edit, base_url))

      assert show_live
             |> form("#base_url-form", base_url: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#base_url-form", base_url: @update_attrs)
        |> render_submit()
        |> follow_redirect(
          conn,
          Routes.base_url_show_path(conn, :show, base_url)
        )

      assert html =~ "Base url updated successfully"
      assert html =~ "some updated url"
    end
  end
end
