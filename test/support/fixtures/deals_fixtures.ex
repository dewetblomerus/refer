defmodule Refer.DealsFixtures do
  alias Refer.AccountsFixtures

  @moduledoc """
  This module defines test helpers for creating
  entities via the `Refer.Deals` context.
  """

  @doc """
  Generate a card.
  """
  def card_fixture(attrs \\ %{}) do
    {:ok, card} =
      attrs
      |> Enum.into(%{
        image_url: "image_url #{Enum.random(0..1_000_000)}",
        title: "title #{Enum.random(0..1_000_000)}"
      })
      |> Refer.Deals.create_card()

    card
  end

  @doc """
  Generate a base_url.
  """
  def base_url_fixture(attrs \\ %{}) do
    {:ok, base_url} =
      attrs
      |> Enum.into(%{
        active: true,
        url: "some url"
      })
      |> Refer.Deals.create_base_url()

    base_url
  end

  @doc """
  Generate a referral_link.
  """
  def referral_link_fixture(attrs \\ %{}) do
    card = card_fixture()
    user = AccountsFixtures.user_fixture()

    {:ok, referral_link} =
      attrs
      |> Enum.into(%{
        card_id: card.id,
        url: "some url",
        user_id: user.id
      })
      |> Refer.Deals.create_referral_link()

    referral_link
  end
end
