defmodule ReferWeb.ConnCase do
  alias Ecto.Adapters.SQL.Sandbox
  alias Refer.{Accounts, AccountsFixtures}

  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use ReferWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import ReferWeb.ConnCase

      alias ReferWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint ReferWeb.Endpoint
    end
  end

  setup tags do
    pid =
      Sandbox.start_owner!(Refer.Repo,
        shared: not tags[:async]
      )

    on_exit(fn -> Sandbox.stop_owner(pid) end)
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  @doc """
  Setup helper that registers and logs in users.

      setup :register_and_log_in_user

  It stores an updated connection and a registered user in the
  test context.
  """
  def register_and_log_in_user(%{conn: conn}) do
    user = Refer.AccountsFixtures.user_fixture()
    %{conn: log_in_user(conn, user), user: user}
  end

  def register_and_log_in_admin(%{conn: conn}) do
    user = Refer.AccountsFixtures.user_fixture(admin: true)
    %{conn: log_in_user(conn, user), user: user}
  end

  def confirm_user(%{conn: conn, user: user}) do
    token =
      AccountsFixtures.extract_user_token(fn url ->
        Accounts.deliver_user_confirmation_instructions(user, url)
      end)

    {:ok, confirmed_user} = Accounts.confirm_user(token)

    %{conn: conn, user: confirmed_user}
  end

  @doc """
  Logs the given `user` into the `conn`.

  It returns an updated `conn`.
  """
  def log_in_user(conn, user) do
    token = Refer.Accounts.generate_user_session_token(user)

    conn
    |> Phoenix.ConnTest.init_test_session(%{})
    |> Plug.Conn.put_session(:user_token, token)
  end
end
