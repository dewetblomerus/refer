defmodule Refer.DealsTest do
  use Refer.DataCase, async: true

  alias Refer.{AccountsFixtures, Deals}

  describe "cards" do
    alias Refer.Deals.Card

    import Refer.DealsFixtures

    @invalid_attrs %{image_url: nil, title: nil}

    test "list_cards/0 returns all cards" do
      card = card_fixture()
      assert Deals.list_cards() == [card]
    end

    test "get_card!/1 returns the card with given id" do
      card = card_fixture()
      assert Deals.get_card!(card.id) == card
    end

    test "create_card/1 with valid data creates a card" do
      valid_attrs = %{image_url: "some image_url", title: "some title"}

      assert {:ok, %Card{} = card} = Deals.create_card(valid_attrs)
      assert card.image_url == "some image_url"
      assert card.title == "some title"
    end

    test "create_card/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Deals.create_card(@invalid_attrs)
    end

    test "update_card/2 with valid data updates the card" do
      card = card_fixture()

      update_attrs = %{
        image_url: "some updated image_url",
        title: "some updated title"
      }

      assert {:ok, %Card{} = card} = Deals.update_card(card, update_attrs)
      assert card.image_url == "some updated image_url"
      assert card.title == "some updated title"
    end

    test "update_card/2 with invalid data returns error changeset" do
      card = card_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Deals.update_card(card, @invalid_attrs)

      assert card == Deals.get_card!(card.id)
    end

    test "delete_card/1 deletes the card" do
      card = card_fixture()
      assert {:ok, %Card{}} = Deals.delete_card(card)
      assert_raise Ecto.NoResultsError, fn -> Deals.get_card!(card.id) end
    end

    test "change_card/1 returns a card changeset" do
      card = card_fixture()
      assert %Ecto.Changeset{} = Deals.change_card(card)
    end
  end

  describe "base_urls" do
    alias Refer.Deals.BaseUrl

    import Refer.DealsFixtures

    @invalid_attrs %{active: nil, url: nil}

    setup do
      [card: card_fixture()]
    end

    test "list_base_urls/0 returns all base_urls" do
      base_url = base_url_fixture()
      assert Deals.list_base_urls() == [base_url]
    end

    test "get_base_url!/1 returns the base_url with given id" do
      base_url = base_url_fixture()
      assert Deals.get_base_url!(base_url.id) == base_url
    end

    test "create_base_url/1 with valid data creates a base_url" do
      valid_attrs = %{active: true, url: "some url"}

      assert {:ok, %BaseUrl{} = base_url} = Deals.create_base_url(valid_attrs)
      assert base_url.active == true
      assert base_url.url == "some url"
    end

    test "create_base_url/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Deals.create_base_url(@invalid_attrs)
    end

    test "update_base_url/2 with valid data updates the base_url" do
      base_url = base_url_fixture()
      update_attrs = %{active: false, url: "some updated url"}

      assert {:ok, %BaseUrl{} = base_url} =
               Deals.update_base_url(base_url, update_attrs)

      assert base_url.active == false
      assert base_url.url == "some updated url"
    end

    test "update_base_url/2 with invalid data returns error changeset" do
      base_url = base_url_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Deals.update_base_url(base_url, @invalid_attrs)

      assert base_url == Deals.get_base_url!(base_url.id)
    end

    test "delete_base_url/1 deletes the base_url" do
      base_url = base_url_fixture()
      assert {:ok, %BaseUrl{}} = Deals.delete_base_url(base_url)

      assert_raise Ecto.NoResultsError, fn ->
        Deals.get_base_url!(base_url.id)
      end
    end

    test "change_base_url/1 returns a base_url changeset" do
      base_url = base_url_fixture()
      assert %Ecto.Changeset{} = Deals.change_base_url(base_url)
    end
  end

  describe "referral_links" do
    alias Refer.Deals.ReferralLink

    import Refer.DealsFixtures

    @invalid_attrs %{url: nil}

    test "list_referral_links/0 returns all referral_links" do
      referral_link = referral_link_fixture()
      assert Deals.list_referral_links() == [referral_link]
    end

    test "list_referral_links_for_card/1 returns all referral_links for a card" do
      card = card_fixture()
      user = AccountsFixtures.user_fixture()
      referral_link = referral_link_fixture(card_id: card.id, user_id: user.id)

      assert Deals.list_referral_links_for_card(card.id) == [
               {referral_link, user.first_name, user.last_name}
             ]
    end

    test "get_referral_link!/1 returns the referral_link with given id" do
      referral_link = referral_link_fixture()
      assert Deals.get_referral_link!(referral_link.id) == referral_link
    end

    test "get_referral_link_by/1 returns the referral_link with given id" do
      user = AccountsFixtures.user_fixture()
      card = card_fixture(%{user_id: user.id})

      referral_link =
        referral_link_fixture(%{user_id: user.id, card_id: card.id})

      assert Deals.get_referral_link_by(user_id: user.id, card_id: card.id) ==
               referral_link
    end

    test "create_referral_link/1 with valid data creates a referral_link" do
      card = card_fixture()
      user = AccountsFixtures.user_fixture()
      valid_attrs = %{url: "some url", card_id: card.id, user_id: user.id}

      assert {:ok, %ReferralLink{} = referral_link} =
               Deals.create_referral_link(valid_attrs)

      assert referral_link.url == "some url"
      assert referral_link.card_id == card.id
    end

    test "create_referral_link/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Deals.create_referral_link(@invalid_attrs)
    end

    test "update_referral_link/2 with valid data updates the referral_link" do
      referral_link = referral_link_fixture()
      update_attrs = %{url: "some updated url"}

      assert {:ok, %ReferralLink{} = referral_link} =
               Deals.update_referral_link(referral_link, update_attrs)

      assert referral_link.url == "some updated url"
    end

    test "update_referral_link/2 with invalid data returns error changeset" do
      referral_link = referral_link_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Deals.update_referral_link(referral_link, @invalid_attrs)

      assert referral_link == Deals.get_referral_link!(referral_link.id)
    end

    test "delete_referral_link/1 deletes the referral_link" do
      referral_link = referral_link_fixture()
      assert {:ok, %ReferralLink{}} = Deals.delete_referral_link(referral_link)

      assert_raise Ecto.NoResultsError, fn ->
        Deals.get_referral_link!(referral_link.id)
      end
    end

    test "change_referral_link/1 returns a referral_link changeset" do
      referral_link = referral_link_fixture()
      assert %Ecto.Changeset{} = Deals.change_referral_link(referral_link)
    end
  end
end
