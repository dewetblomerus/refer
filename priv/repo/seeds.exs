alias NimbleCSV.RFC4180, as: CSV

"priv/repo/card_seeds.csv"
|> File.stream!()
|> CSV.parse_stream()
|> Stream.map(fn [title, image_url] ->
  card_map = %{title: :binary.copy(title), image_url: :binary.copy(image_url)}
  Refer.Deals.create_card(card_map)
end)
|> Stream.run()

user_attrs = %{
  email: "dewetblomerus@gmail.com",
  password: "asdf;lkjasdf;lkj",
  first_name: "De Wet",
  last_name: "Blomerus"
}

Refer.Accounts.register_user(user_attrs)
