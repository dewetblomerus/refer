defmodule Refer.Repo.Migrations.CreateReferralLinks do
  use Ecto.Migration

  def change do
    create table(:referral_links) do
      add :url, :string, null: false
      add :base_url_id, references(:base_urls, on_delete: :nilify_all)
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :card_id, references(:cards, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:referral_links, [:base_url_id])
    create index(:referral_links, [:user_id])
    create index(:referral_links, [:card_id])
  end
end
