defmodule Refer.Repo.Migrations.CreateBaseUrls do
  use Ecto.Migration

  def change do
    create table(:base_urls) do
      add :url, :string, null: false
      add :active, :boolean, default: false, null: false

      timestamps()
    end
  end
end
