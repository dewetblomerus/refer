defmodule Refer.Repo.Migrations.AlterCardsUnique do
  use Ecto.Migration

  def change do
    create unique_index(:cards, :title)
    create unique_index(:cards, :image_url)
  end
end
