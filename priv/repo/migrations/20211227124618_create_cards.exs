defmodule Refer.Repo.Migrations.CreateCards do
  use Ecto.Migration

  def change do
    create table(:cards) do
      add :image_url, :string
      add :title, :string, null: false

      timestamps()
    end
  end
end
