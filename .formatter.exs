[
  plugins: [Phoenix.LiveView.HTMLFormatter],
  import_deps: [:ecto, :phoenix],
  inputs:
    Enum.flat_map(
      [
        "*.{heex,ex,exs}",
        "priv/*/seeds.exs",
        "{config,lib,test}/**/*.{heex,ex,exs}"
      ],
      &Path.wildcard(&1, match_dot: true)
    ) -- ["lib/refer_web/templates/layout/live.html.heex"],
  subdirectories: ["priv/*/migrations"],
  line_length: 80
]
